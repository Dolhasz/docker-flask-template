FROM python:3.7.2-slim

COPY . /app
WORKDIR /app

ENV FLASK_ENV=development
ENV FLASK_APP=base_api.py

RUN pip install numpy   
RUN pip install pandas


RUN pip install -r requirements.txt

CMD ["flask", "run", "--host", "0.0.0.0", "--port", "8888"]
