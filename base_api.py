from flask import Flask, request
import pandas as pd
import json

app = Flask(__name__)

DATA = None
  
@app.route('/deeplens', methods=["GET", "POST"])
def receive_deeplens_results():
    global DATA
    if request.method == "POST":
        print(request.data)
        DATA = request.get_json()
        return 'success'
    elif request.method == 'GET':
        return DATA